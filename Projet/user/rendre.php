<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <td>ID Materiels</td>
          <td>NOM Materiels</td>
          <td>TYPE Materiels</td>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php
          require('../traitement/bdd.php');
          $sql="SELECT no_mat, nom_mat, type_mat FROM materiels, emprunt WHERE no_mat=no_emp";
          $req=$bdd->prepare($sql);
          $req->execute();
          foreach ($req as $value) {
            echo "
            <tr>
              <td>$value[no_mat]</td>
              <td>$value[nom_mat]</td>
              <td>$value[type_mat]</td>
              <td><a href='../traitement/rendre_traitement.php?id=$value[no_mat]'>Rendre</a></td>
            </tr>
            ";
          }
        ?>
      </tbody>
    </table>
  </body>
</html>
