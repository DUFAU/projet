<?php
  require('../traitement/bdd.php');
  $sql="INSERT INTO materiels (no_mat, nom_mat, type_mat, dispo_mat) VALUES (:id,:nom,:type,:etat)";
  $req=$bdd->prepare($sql);
  $req->execute(array(
    'id' => $_POST['id'],
    'nom' => $_POST['nom'],
    'type' => $_POST['type'],
    'etat' => $_POST['etatlist']
  ));
  header("Location:../admin.php");
?>
