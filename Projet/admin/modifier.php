<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
      require('../traitement/bdd.php');
      $sql="SELECT no_mat, nom_mat, type_mat, dispo_mat FROM materiels WHERE no_mat=:id";
      $req=$bdd->prepare($sql);
      $req->execute(array(
        'id' => $_GET['id']
      ));

    echo "<form action='modifier_traitement.php?id=$_GET[id]' method='post'>";
    ?>
      <table>
        <thead>
          <tr>
            <td>ID Materiels</td>
            <td>NOM Materiels</td>
            <td>TYPE Materiels</td>
            <td>Etat</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?php
              foreach ($req as $value) {
                echo "
                <td><input type='number' name='id' value='$value[no_mat]'></td>
                <td><input type='text' name='nom' value='$value[nom_mat]'></td>
                <td><input type='text' name='type' value='$value[type_mat]'></td>
                <td>
                  <select class='etat' name='etatlist'>
                    <option value=1>Disponible</option>
                    <option value=0>Non-Disponible</option>
                  </select>
                </td>
                ";
              }
            ?>

            <td><input type="submit"></td>
          </tr>
        </tbody>
      </table>
    </form>
  </body>
</html>
