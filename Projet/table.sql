DROP DATABASE M2105;
CREATE DATABASE M2105;

use M2105;

CREATE TABLE Utilisateur(
  no_utilisateur INT NOT NULL PRIMARY KEY,
  prenom_utilisateur VARCHAR (25) NOT NULL,
  nom_utilisateur VARCHAR (25) NOT NULL
);

CREATE TABLE Materiels(
  no_mat INT NOT NULL PRIMARY KEY,
  nom_mat VARCHAR (25) NOT NULL,
  type_mat VARCHAR (25) NOT NULL,
  dispo_mat BOOLEAN DEFAULT true
);

CREATE TABLE Emprunt(
  no_emp INT NOT NULL,
  no_utilisateur INT NOT NULL
);

INSERT INTO Utilisateur (no_utilisateur, nom_utilisateur, prenom_utilisateur) VALUES
  (37000233, 'BLANCO', 'Anthony'),
  (37000256,'COSSU', 'Julien'),
  (37000320, 'RIVIERE', 'Mathias'),
  (37000404, 'CHEVALIER', 'Franck'),
  (37000974, 'DUFAU', 'Hugo'),
  (37000123, 'DIJOUX', 'Prescilla');

INSERT INTO Materiels (no_mat, nom_mat, type_mat, dispo_mat) VALUES

  #*****************************************************Connecteurs*****************************************************
  (1101, 'Connecteur', 'Cable RJ-45', true),
  (1102, 'Connecteur', 'Cable RJ-45', true),
  (1103, 'Connecteur', 'Cable RJ-45', true),
  (1104, 'Connecteur', 'Cable RJ-45', false),
  (1105, 'Connecteur', 'Cable RJ-45', false),

  (1201, 'Connecteur', 'Cable HDMI-VGA', true),
  (1202, 'Connecteur', 'Cable HDMI-VGA', false),
  (1203, 'Connecteur', 'Cable HDMI-VGA', true),
  (1204, 'Connecteur', 'Cable HDMI-VGA', false),
  (1205, 'Connecteur', 'Cable HDMI-VGA', true),

  (1301, 'Connecteur', 'Cable USB-USB', true),
  (1302, 'Connecteur', 'Cable USB-USB', true),
  (1303, 'Connecteur', 'Cable USB-USB', false),
  (1304, 'Connecteur', 'Cable USB-USB', false),
  (1305, 'Connecteur', 'Cable USB-USB', false),

  (1401, 'Connecteur', 'Cable DVI-VGA', true),
  (1402, 'Connecteur', 'Cable DVI-VGA', true),
  (1403, 'Connecteur', 'Cable DVI-VGA', false),
  (1404, 'Connecteur', 'Cable DVI-VGA', true),
  (1405, 'Connecteur', 'Cable DVI-VGA', true);
