<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Utilisateur</title>
  </head>
  <body>
    <center>
        <br>
    <table>
      <thead>
        <tr>
          <td>ID Materiels</td>
          <td>NOM Materiels</td>
          <td>TYPE Materiels</td>
          <td>Disponibilité</td>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php
          require('traitement/bdd.php');
          $sql="SELECT no_mat, nom_mat, type_mat, dispo_mat FROM materiels WHERE dispo_mat=1";
          $req=$bdd->query($sql);
          foreach ($req as $value) {
            echo "
            <tr>
              <td>$value[no_mat]</td>
              <td>$value[nom_mat]</td>
              <td>$value[type_mat]</td>
              <td>$value[dispo_mat]</td>
              <td>
                <a href='admin/modifier.php?id=$value[no_mat]'>Modifier</a>
                <a href='admin/supprimer.php?id=$value[no_mat]'>Supprimer</a>
              </td>
            </tr>
            ";
          }
        ?>
        <a href="admin/ajouter.php"> <h1> Ajouter un materiel</h1> </a> <br> <br>
      </tbody>
    </table>

  </body>
</html>
